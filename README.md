# Traffic Watch App #

Small app that shows you the current incidents on German streets and Autobahn in a table view. Detailed information about each incident are presented on a Map interface.

### How to run ###

Run using Xcode 7.2 on an iPhone.