//
//  IncidentsParser.swift
//  Traffic Watch
//
//  Created by Lars Lütjens on 16/04/16.
//  Copyright © 2016 TUM LS1. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class IncidentsParser: NSObject {
    var incidentsParserDelegate: IncidentsParserDelegate?
    
	typealias JSONObject = [String: AnyObject]

	private var requestURL: NSURL!
	private var session: NSURLSession!
	private var task: NSURLSessionDownloadTask!

	var json: JSONObject?

	init(url: NSURL) {
		super.init()
		self.requestURL = url
		let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
		session = NSURLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
	}

	func startDownload() {
		UIApplication.sharedApplication().networkActivityIndicatorVisible = true
		task = session.downloadTaskWithURL(requestURL)
		task.resume()
	}

	private func parseIncidents(json: JSONObject) -> [Incident] {
		var parsedIncidents = [Incident]()
		guard let rootObject = json["TicXML"] as? JSONObject, incidents = rootObject["message"] as? [JSONObject] else {
			print("[\(self.dynamicType)] Error parsing incidents")
			return parsedIncidents
		}

		for incident in incidents {
			if let incident = incidentFromJSONObject(incident) {
				parsedIncidents.append(incident)
				print(incident)
			}
		}
		return parsedIncidents
	}

	private func incidentFromJSONObject(jsonObject: JSONObject) -> Incident? {
		guard let incidentAttributes = jsonObject["@attributes"] as? JSONObject,
			identifier = incidentAttributes["NUM"] as? String,
			rawType = incidentAttributes["TYP"] as? String,
			type = IncidentType(rawValue: rawType),
			title = jsonObject["MES0"] as? String ?? Optional(""),
			subtitle = jsonObject["MES2"] as? String ?? Optional(""),
            rawTimestamp = jsonObject["UPT"] as? String,
            timestamp = Incident.timestampFormatter.dateFromString(rawTimestamp) else
		{
			print("[\(self.dynamicType)] Error parsing incident")
			return nil
		}
        
        var coordinates = [CLLocationCoordinate2D]()
        if let rawCoordinates = jsonObject["wgs84"] as? JSONObject {
            coordinates = coordinatesFromJSONObjects([rawCoordinates])
        } else if let rawCoordinates = jsonObject["wgs84"] as? [JSONObject] {
            coordinates = coordinatesFromJSONObjects(rawCoordinates)
        }

		let cleanedSubtitle = subtitle.stringByReplacingOccurrencesOfString("\n", withString: " ")
		return Incident(identifier: identifier, title: title, subtitle: cleanedSubtitle, type: type, timestamp: timestamp, coordinates: coordinates)
	}
    
    func coordinatesFromJSONObjects(jsonObjects: [JSONObject]) -> [CLLocationCoordinate2D] {
        var coordinates = [CLLocationCoordinate2D]()
        for jsonObject in jsonObjects {
            guard let attributes = jsonObject["@attributes"] as? JSONObject,
                longitudeString = attributes["lng"] as? String,
                latitudeString = attributes["lat"] as? String,
                longitude = Double(longitudeString),
                latitude = Double(latitudeString) else {
                    print("[\(self.dynamicType)] Error parsing coordinate")
                    continue
            }
            coordinates.append(CLLocationCoordinate2D(latitude: latitude, longitude:
                longitude))
        }
        
        return coordinates
    }
}

extension IncidentsParser: NSURLSessionTaskDelegate {
	func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
		UIApplication.sharedApplication().networkActivityIndicatorVisible = false
		if let error = error {
			print("[\(self.dynamicType)] Error: \(error.localizedDescription)")
		}
	}
}

extension IncidentsParser: NSURLSessionDownloadDelegate {
	func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
		guard let response = downloadTask.response as? NSHTTPURLResponse, let data = NSData(contentsOfURL: location)
		where response.statusCode == 200 else {
            print ("Wrong answer from server.")
			return
		}

		do {
			json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? JSONObject
            self.incidentsParserDelegate?.incidentsParser(self, loadedIncidents: parseIncidents(json!))
            
		} catch {
			print("[\(self.dynamicType)] Error reading JSON object\n", error)
		}
	}
}

protocol IncidentsParserDelegate: class {
    func incidentsParser(parser: IncidentsParser, loadedIncidents: [Incident])
}
