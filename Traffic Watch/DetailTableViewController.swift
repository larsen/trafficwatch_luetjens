//
//  DetailViewController.swift
//  Traffic Watch
//
//  Created by Lars Lütjens on 17/04/16.
//  Copyright © 2016 TUM LS1. All rights reserved.
//

import UIKit
import MapKit

class DetailTableViewController: UITableViewController {
	var incident: Incident?

	@IBOutlet var map: MKMapView!
	@IBOutlet var identifierLabel: UILabel!
	@IBOutlet var subtitleLabel: UILabel!
	@IBOutlet var typeLabel: UILabel!
	@IBOutlet var timestampLabel: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()

		map.delegate = self
        
		initializeTableView()
	}

	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		guard indexPath.row == 1 else {
			return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
		}
		return UITableViewAutomaticDimension
	}

	private func initializeTableView() {
		tableView.estimatedRowHeight = 44.0

		guard let incident = incident else {
			identifierLabel.text = ""
			title = ""
			subtitleLabel.text = ""
			typeLabel.text = ""
			timestampLabel.text = ""

			return
		}

		identifierLabel.text = incident.identifier
		title = incident.title
		subtitleLabel.text = incident.subtitle
		typeLabel.text = String(incident.type)
		timestampLabel.text = Incident.timestampFormatter.stringFromDate(incident.timestamp)
        
        centerMap(incident.coordinates)
        addOverlay(incident.coordinates)
	}

	private func centerMap(coordinates: [CLLocationCoordinate2D]) {
		guard let maxLatitude = coordinates.map({ $0.latitude }).maxElement(),
			minLatitude = coordinates.map({ $0.latitude }).minElement(),
			maxLongitude = coordinates.map({ $0.longitude }).maxElement(),
			minLongitude = coordinates.map({ $0.longitude }).minElement() else {
				return
		}
		let centerCoordinate = CLLocationCoordinate2D(
			latitude: (maxLatitude + minLatitude) / 2,
			longitude: (maxLongitude + minLongitude) / 2
		)
		let latitudeDelta = (abs(maxLatitude) - abs(minLatitude)) * 1.1
		let longitudeDelta = (abs(maxLongitude) - abs(minLongitude)) * 1.1
		let span = MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
		map.region = MKCoordinateRegion(center: centerCoordinate, span: span)
	}
    
    private func addOverlay(coordinates: [CLLocationCoordinate2D]) {
        var mutableCoordinates = coordinates
        let polyLine = MKPolyline(coordinates: &mutableCoordinates, count: coordinates.count)
        map.addOverlay(polyLine)
    }
}

extension DetailTableViewController: MKMapViewDelegate {
	func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
		guard overlay is MKPolyline else {
			return MKOverlayRenderer(overlay: overlay)
		}
		let lineRenderer = MKPolylineRenderer(overlay: overlay)
		lineRenderer.strokeColor = UIColor.redColor().colorWithAlphaComponent(0.6)
		return lineRenderer
	}
}
