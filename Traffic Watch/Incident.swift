//
//  Incident.swift
//  Traffic Watch
//
//  Created by Lars Lütjens on 16/04/16.
//  Copyright © 2016 TUM LS1. All rights reserved.
//

import Foundation
import CoreLocation

enum IncidentType: String {
	case Congestion = "TST"
	case SpeedTrap = "TBL"
}

struct Incident {

	// MARK: Formatters
	static let timestampFormatter: NSDateFormatter = {
		let timestampFormatter = NSDateFormatter()

		let format = "yyyy-MM-dd HH:mm:ss"
		timestampFormatter.dateFormat = format

		return timestampFormatter
	}()

	// MARK: Properties
	let identifier: String
	let title: String
	let subtitle: String
	let type: IncidentType
	let timestamp: NSDate
	let coordinates: [CLLocationCoordinate2D]
}

extension Incident: CustomStringConvertible {
	var description: String {
		return "<Incident: identifier = \(self.identifier), title = \(self.title), subtitle = \(self.subtitle), type = \(self.type)>"
	}
}
