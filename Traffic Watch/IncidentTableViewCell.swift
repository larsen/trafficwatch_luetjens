//
//  IncidentTableViewCell.swift
//  Traffic Watch
//
//  Created by Lars Lütjens on 17/04/16.
//  Copyright © 2016 TUM LS1. All rights reserved.
//

import UIKit

class IncidentTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    

}
