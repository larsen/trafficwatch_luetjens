//
//  TableViewController.swift
//  Traffic Watch
//
//  Created by Lars Lütjens on 16/04/16.
//  Copyright © 2016 TUM LS1. All rights reserved.
//

import UIKit

class IncidentsTableViewController: UITableViewController {
    
    
    private var incidentsParser: IncidentsParser?
    private var incidents: [Incident]?
    private var dynamicTitle: String {
        let number = incidents?.count ?? 0
        return "\(number) Incidents"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Automatically resize cell to dimensions
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        
        refreshIncidents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func refreshButtonClicked(sender: UIBarButtonItem) {
        // TODO: let refreshing button spin while refreshing
        refreshIncidents()
        
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incidents?.count ?? 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("incidentCell", forIndexPath: indexPath) as! IncidentTableViewCell
        
        // Configure the cell..
        guard let incidents = incidents else {
            return cell
        }
        cell.titleLabel?.text = incidents[indexPath.row].title
        cell.subtitleLabel?.text = incidents[indexPath.row].subtitle
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        guard let incidents = incidents, currentlySelectedRow = tableView.indexPathForSelectedRow?.row else {
            return
        }
        let currentlySelectedIncident = incidents[currentlySelectedRow]
        let detailViewController = segue.destinationViewController as? DetailTableViewController
        
        // Pass the selected object to the new view controller.
        detailViewController?.incident = currentlySelectedIncident
    }
    
    func refreshIncidents() {
        let incidentsURL = NSURL(string: "https://bit.ly/trafficWatch")!
        incidentsParser = IncidentsParser(url: incidentsURL)
        incidentsParser?.incidentsParserDelegate = self
        incidentsParser!.startDownload()
    }

}

extension IncidentsTableViewController: IncidentsParserDelegate {
    func incidentsParser(parser: IncidentsParser, loadedIncidents: [Incident]) {
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.incidents = loadedIncidents
            self.title = self.dynamicTitle
            self.tableView.reloadData()
        })
    }
}